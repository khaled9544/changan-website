import { Component, OnInit } from '@angular/core';
import { selectedAnimation } from '../animations/selectAnimation';
import { offer, Offers } from '../shared/offers';


@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.css'],
  animations:[selectedAnimation]
})
export class SlidesComponent implements OnInit {

  offers:Array<offer> = Offers;
  selected: string = "CS30";

  constructor() { }

  ngOnInit() {
  }

  changeSelected(title:string){
    this.selected = title;
  }

}
