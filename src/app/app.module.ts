import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './shared/router';

// PLUGINS
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { CollapseModule } from 'ngx-bootstrap/collapse';


// COMPONENTS
import { BannerComponent } from './banner/banner.component';
import { HeaderComponent } from './header/header.component';
import { SlidesComponent } from './slides/slides.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    BannerComponent,
    HeaderComponent,
    SlidesComponent
  ],
  
  imports: [
    CollapseModule.forRoot(),
    RouterModule.forRoot(appRoutes,{ enableTracing: true }),
    Angular2FontawesomeModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
