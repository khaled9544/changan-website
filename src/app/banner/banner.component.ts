import { Component, OnInit } from '@angular/core';
import { section, navSections } from '../shared/sections';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  navArray:Array<section> = navSections;
  isCollapsed: boolean = true;

  constructor() { 

  }

  ngOnInit() {
  }

}
