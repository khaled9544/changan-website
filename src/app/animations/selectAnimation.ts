import {
  trigger,
  state,
  style,
  transition,
  animate,

} from '@angular/animations';

export const selectedAnimation =
  trigger('select', [
    state('*', style({
      "flex": "1"
    })),
    state('selected', style({
      "flex": "2"
    })),
    transition('* <=> *', [
        animate('700ms ease-in'),
    ])
  ]);
