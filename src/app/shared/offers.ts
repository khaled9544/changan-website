export class offer {
  title: string;
  specs: string;
  image: string;
  caption: string;
}

export const Offers: Array < offer > = [{
    title: "CS15",
    specs: `
    <ul class="specs">
  <li>
    TWIN EXHUAST
  </li>
  <li>
    SUNROOF
  </li>
  <li>
    BLUETOOTH 2.0
  </li>
  <li>
    6 AIRBAGS
  </li>
  <li>
    LED LIGHTS
  </li>
  <li>
    NIGHT FOG
  </li>
</ul>
    `,
    image: "assets/images/offer1.png",
    caption: "The Beast"
  },
  {
    title: "CS30",
    specs: `
    <ul class="specs">
  <li>
    TWIN EXHUAST
  </li>
  <li>
    SUNROOF
  </li>
  <li>
    BLUETOOTH 2.0
  </li>
  <li>
    6 AIRBAGS
  </li>
  <li>
    LED LIGHTS
  </li>
  <li>
    NIGHT FOG
  </li>
</ul>`,
    image: "assets/images/offer2.png",
    caption: "The Beast"
  },
  {
    title: "CS75",
    specs: `
    <ul class="specs">
  <li>
    TWIN EXHUAST
  </li>
  <li>
    SUNROOF
  </li>
  <li>
    BLUETOOTH 2.0
  </li>
  <li>
    6 AIRBAGS
  </li>
  <li>
    LED LIGHTS
  </li>
  <li>
    NIGHT FOG
  </li>
</ul>`,
    image: "assets/images/offer3.png",
    caption: "The Beast"

  }
]
