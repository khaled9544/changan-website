export class section {
  name: string;
}

export const navSections: Array < section > = [{
  name: "Home"
}, {
  name: "Vehicles"
}, {
  name: "About Changan"
}, {
  name: "Contact Us"
}, {
  name: "Book a Test Drive"
}, {
  name: "Changan Dealers"
}]
